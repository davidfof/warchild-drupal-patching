#!/usr/bin/sh

# 1. Disable all cron jobs
sudo service cron stop

# 2. Update debian archive repositories for the package manager
> /etc/apt/sources.list
echo "deb http://archive.debian.org/debian squeeze main" >> /etc/apt/sources.list

# 3. Upgrade to php 5.4

# Add repos to install PHP 5.4
echo "deb http://packages.dotdeb.org squeeze all" >> /etc/apt/sources.list
echo "deb-src http://packages.dotdeb.org squeeze all" >> /etc/apt/sources.list
echo "deb http://packages.dotdeb.org squeeze-php54 all" >> /etc/apt/sources.list
echo "deb-src http://packages.dotdeb.org squeeze-php54 all" >> /etc/apt/sources.list

# Install php
curl http://www.dotdeb.org/dotdeb.gpg | sudo apt-key add -
sudo apt-get update
sudo apt-get -y -f dist-upgrade
sudo rm /etc/php5/conf.d/suhosin.ini
sudo rm /etc/php5/conf.d/apc.ini
sudo apt-get -y install php5-apc

# 4. Update openSSL

# Update openSSL to 1.0.2h
wget ftp://openssl.org/source/openssl-1.0.2h.tar.gz
tar -xvzf openssl-1.0.2h.tar.gz
rm -rf openssl-1.0.2h.tar.gz
cd openssl-1.0.2h
./config --prefix=/usr/
make depend
make && make install


# Install composer
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php -r "if (hash_file('SHA384', 'composer-setup.php') === '070854512ef404f16bac87071a6db9fd9721da1684cd4589b1196c3faf71b9a2682e2311b36a5079825e155ac7ce150d') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
php composer-setup.php
php -r "unlink('composer-setup.php');"

mv composer.phar /usr/local/bin/composer
echo export PATH="$HOME/.composer/vendor/bin:$PATH" >> ~/.bashrc

# Install drush
composer global require drush/drush:7.1.0

# Backup database before updating

# Update modules
cd /var/www/public_html/sites/warchild.org.uk/
drush up -y block_class boost features

# Re-enable cron jobs
sudo service cron start
